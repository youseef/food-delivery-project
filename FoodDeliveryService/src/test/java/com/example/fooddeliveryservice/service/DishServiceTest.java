package com.example.fooddeliveryservice.service;

import com.example.fooddeliveryservice.entity.Dish;
import com.example.fooddeliveryservice.entity.dtos.DishDTO;
import com.example.fooddeliveryservice.exceptions.ResourceNotFoundException;
import com.example.fooddeliveryservice.repository.DishRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DishServiceTest {

    @Mock
    private DishRepository dishRepository;

    @InjectMocks
    private DishService dishService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddDish() {
        DishDTO dishDTO = new DishDTO();
        dishDTO.setName("Pasta");
        dishDTO.setPrice(12.5);

        dishService.addDish(dishDTO);

        verify(dishRepository, times(1)).save(any(Dish.class));
    }

    @Test
    public void testUpdateDish_Success() {
        Long dishId = 1L;
        DishDTO dishDTO = new DishDTO();
        dishDTO.setName("Updated Dish");
        dishDTO.setPrice(15.0);

        when(dishRepository.updateDishById(dishId, dishDTO)).thenReturn(1);

        dishService.updateDish(dishId, dishDTO);

        verify(dishRepository, times(1)).updateDishById(dishId, dishDTO);
    }

    @Test
    public void testUpdateDish_NotFound() {
        Long dishId = 1L;
        DishDTO dishDTO = new DishDTO();
        dishDTO.setName("Updated Dish");
        dishDTO.setPrice(15.0);

        when(dishRepository.updateDishById(dishId, dishDTO)).thenReturn(0);

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class,
                () -> dishService.updateDish(dishId, dishDTO));

        assertEquals("Dish not found with id " + dishId, exception.getMessage());

        verify(dishRepository, times(1)).updateDishById(dishId, dishDTO);
    }

    @Test
    public void testDeleteDish() {
        Long dishId = 1L;

        dishService.deleteDish(dishId);

        verify(dishRepository, times(1)).deleteById(dishId);
    }

    @Test
    public void testGetAllDishes() {
        List<Dish> mockDishes = Arrays.asList(
                new Dish(1L, "Pizza", 10.0),
                new Dish(2L, "Burger", 8.5)
        );

        when(dishRepository.findAll()).thenReturn(mockDishes);

        List<DishDTO> dishes = dishService.getAllDishes();

        assertEquals(2, dishes.size());
        assertEquals("Pizza", dishes.get(0).getName());
        assertEquals(Double.valueOf(10.0), dishes.get(0).getPrice());
        assertEquals("Burger", dishes.get(1).getName());
        assertEquals(Double.valueOf(8.5), dishes.get(1).getPrice());

        verify(dishRepository, times(1)).findAll();
    }

    @Test
    public void testGetDishById_Success() {
        Long dishId = 1L;
        Dish mockDish = new Dish(dishId, "Steak", 20.0);

        when(dishRepository.findById(dishId)).thenReturn(Optional.of(mockDish));

        DishDTO dishDTO = dishService.getDishById(dishId);

        assertEquals("Steak", dishDTO.getName());
        assertEquals(Double.valueOf(20.0), dishDTO.getPrice());

        verify(dishRepository, times(1)).findById(dishId);
    }

    @Test
    public void testGetDishById_NotFound() {
        Long dishId = 1L;

        when(dishRepository.findById(dishId)).thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class,
                () -> dishService.getDishById(dishId));

        assertEquals("Dish not found with id " + dishId, exception.getMessage());

        verify(dishRepository, times(1)).findById(dishId);
    }
}
