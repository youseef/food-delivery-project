package com.example.fooddeliveryservice.service;

import com.example.fooddeliveryservice.entity.Orders;
import com.example.fooddeliveryservice.entity.dtos.DishDTO;
import com.example.fooddeliveryservice.entity.dtos.OrderRequestDTO;
import com.example.fooddeliveryservice.entity.dtos.OrderResponseDTO;
import com.example.fooddeliveryservice.entity.enums.Status;
import com.example.fooddeliveryservice.exceptions.ResourceNotFoundException;
import com.example.fooddeliveryservice.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPlaceOrder() {
        OrderRequestDTO orderRequestDTO = createOrderRequestDTO();

        orderService.placeOrder(orderRequestDTO);

        verify(orderRepository, times(1)).save(any(Orders.class));
    }

    @Test
    public void testGetAllOrders() {
        List<Orders> mockOrders = createMockOrders();

        when(orderRepository.findAll()).thenReturn(mockOrders);

        List<OrderResponseDTO> orders = orderService.getAllOrders();

        assertEquals(2, orders.size());
        assertEquals(Status.PLACED, orders.get(0).getStatus());
        assertEquals(Status.PLACED, orders.get(1).getStatus());

        verify(orderRepository, times(1)).findAll();
    }

    @Test
    public void testUpdateOrderStatus_Success() {
        Long orderId = 1L;
        Status newStatus = Status.DELIVERED;

        Orders mockOrder = new Orders();
        mockOrder.setId(orderId);
        mockOrder.setStatus(Status.PLACED);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(mockOrder));

        orderService.updateOrderStatus(orderId, newStatus);

        assertEquals(newStatus, mockOrder.getStatus());

        verify(orderRepository, times(1)).findById(orderId);
        verify(orderRepository, times(1)).save(mockOrder);
    }

    @Test
    public void testUpdateOrderStatus_NotFound() {
        Long orderId = 1L;
        Status newStatus = Status.DELIVERED;

        when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class,
                () -> orderService.updateOrderStatus(orderId, newStatus));

        assertEquals("Order not found with id " + orderId, exception.getMessage());

        verify(orderRepository, times(1)).findById(orderId);
        verify(orderRepository, never()).save(any(Orders.class));
    }

    @Test
    public void testGetOrderById_Success() {
        Long orderId = 1L;
        Orders mockOrder = new Orders();
        mockOrder.setId(orderId);
        mockOrder.setStatus(Status.PLACED);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(mockOrder));

        OrderResponseDTO orderDTO = orderService.getOrderById(orderId);

        assertEquals(orderId, orderDTO.getId());
        assertEquals(Status.PLACED, orderDTO.getStatus());

        verify(orderRepository, times(1)).findById(orderId);
    }

    @Test
    public void testGetOrderById_NotFound() {
        Long orderId = 1L;

        when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class,
                () -> orderService.getOrderById(orderId));

        assertEquals("Order not found with id " + orderId, exception.getMessage());

        verify(orderRepository, times(1)).findById(orderId);
    }

    private OrderRequestDTO createOrderRequestDTO() {
        OrderRequestDTO orderRequestDTO = new OrderRequestDTO();
        orderRequestDTO.setId(1L);
        orderRequestDTO.setDish(createDishDTOList());
        orderRequestDTO.setTotalCost(50.0);
        orderRequestDTO.setDistance(10);

        return orderRequestDTO;
    }

    private List<DishDTO> createDishDTOList() {
        List<DishDTO> dishDTOList = new ArrayList<>();
        dishDTOList.add(new DishDTO(1L, "Pizza", 10.0));
        dishDTOList.add(new DishDTO(2L, "Burger", 8.5));
        return dishDTOList;
    }

    private List<Orders> createMockOrders() {
        List<Orders> orders = new ArrayList<>();
        Orders order1 = new Orders();
        order1.setId(1L);
        order1.setStatus(Status.PLACED);
        Orders order2 = new Orders();
        order2.setId(2L);
        order2.setStatus(Status.PLACED);
        orders.add(order1);
        orders.add(order2);
        return orders;
    }
}
