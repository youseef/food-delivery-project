package com.example.fooddeliveryservice.service;

import com.example.fooddeliveryservice.entity.Orders;
import com.example.fooddeliveryservice.entity.dtos.OrderRequestDTO;
import com.example.fooddeliveryservice.entity.dtos.OrderResponseDTO;
import com.example.fooddeliveryservice.entity.enums.Status;
import com.example.fooddeliveryservice.exceptions.ResourceNotFoundException;
import com.example.fooddeliveryservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    private static final int PREPARATION_TIME_PER_4_MEALS = 5;
    private static final int DELIVERY_TIME_PER_KM = 3;

    public void placeOrder(OrderRequestDTO orderRequestDTO) {
        Orders orders = convertToEntity(orderRequestDTO);
        int totalNumberOfDishes = orderRepository.findAll().stream()
                .filter(order -> order.getStatus() == Status.PLACED)
                .mapToInt(order -> order.getDish().size())
                .sum();
        int preparationTime = (int) Math.ceil((double) totalNumberOfDishes / 4) * PREPARATION_TIME_PER_4_MEALS;
        int deliveryTime = orderRequestDTO.getDistance() * DELIVERY_TIME_PER_KM;

        orders.setEstimatedPreparationTime(preparationTime);
        orders.setEstimatedDeliveryTime(deliveryTime);
        orders.setStatus(Status.PLACED);

        orderRepository.save(orders);
    }

    public List<OrderResponseDTO> getAllOrders() {
        List<Orders> orders = orderRepository.findAll();
        return orders.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public void updateOrderStatus(Long id, Status status) {
        Orders orders = orderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Order not found with id " + id));

        orders.setStatus(status);
        orderRepository.save(orders);
    }
    public OrderResponseDTO getOrderById(Long id){
        Orders orders = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order not found with id " + id));
        return convertToDto(orders);
    }
    private Orders convertToEntity(OrderRequestDTO orderRequestDTO) {
        Orders orders = new Orders();
        orders.setId(orderRequestDTO.getId());
        orders.setTotalCost(orderRequestDTO.getTotalCost());
        orders.setDistance(orderRequestDTO.getDistance());
        orders.setDish(orderRequestDTO.getDish());

        return orders;
    }

    private OrderResponseDTO convertToDto(Orders orders) {
        OrderResponseDTO orderRequestDTO = new OrderResponseDTO();
        orderRequestDTO.setId(orders.getId());
        orderRequestDTO.setTotalCost(orders.getTotalCost());
        orderRequestDTO.setStatus(orders.getStatus());
        orderRequestDTO.setDistance(orders.getDistance());
        orderRequestDTO.setEstimatedPreparationTime(orders.getEstimatedPreparationTime());
        orderRequestDTO.setEstimatedDeliveryTime(orders.getEstimatedDeliveryTime());
        orderRequestDTO.setDish(orders.getDish());

        return orderRequestDTO;
    }
}
