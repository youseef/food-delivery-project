package com.example.fooddeliveryservice.service;

import com.example.fooddeliveryservice.entity.Dish;
import com.example.fooddeliveryservice.entity.dtos.DishDTO;
import com.example.fooddeliveryservice.exceptions.ResourceNotFoundException;
import com.example.fooddeliveryservice.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DishService {
    @Autowired
    private DishRepository dishRepository;
    public void addDish(DishDTO dishDTO){
        Dish dish = new Dish();
        dish.setName(dishDTO.getName());
        dish.setPrice(dishDTO.getPrice());
        dishRepository.save(dish);
    }
    public void updateDish(Long id, DishDTO dishDTO){
        int updatedRows = dishRepository.updateDishById(id, dishDTO);
        if (updatedRows == 0) {
            throw new ResourceNotFoundException("Dish not found with id " + id);
        }
    }
    public void deleteDish(Long id){
        dishRepository.deleteById(id);
    }
    public List<DishDTO> getAllDishes(){
        List<Dish> dishes = dishRepository.findAll();
        return dishes.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());    }

    public DishDTO getDishById(Long id){
        Dish dish = dishRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Dish not found with id " + id));
        return convertToDto(dish);    }
    private DishDTO convertToDto(Dish dish) {
        DishDTO dishDTO = new DishDTO();
        dishDTO.setName(dish.getName());
        dishDTO.setPrice(dish.getPrice());
        return dishDTO;
    }
}

