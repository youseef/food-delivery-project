package com.example.fooddeliveryservice.repository;

import com.example.fooddeliveryservice.entity.Dish;
import com.example.fooddeliveryservice.entity.dtos.DishDTO;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DishRepository extends JpaRepository<Dish, Long> {
    @Modifying
    @Transactional
    @Query("UPDATE Dish d SET d.name = :#{#dishDTO.name}, d.price = :#{#dishDTO.price} WHERE d.id = :id")
    int updateDishById(@Param("id") Long id, @Param("dishDTO") DishDTO dishDTO);
}
