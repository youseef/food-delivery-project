    package com.example.fooddeliveryservice.repository;

    import com.example.fooddeliveryservice.entity.Orders;
    import com.example.fooddeliveryservice.entity.enums.Status;
    import jakarta.transaction.Transactional;
    import org.springframework.data.jpa.repository.JpaRepository;
    import org.springframework.data.jpa.repository.Modifying;
    import org.springframework.data.jpa.repository.Query;
    import org.springframework.data.repository.query.Param;

    public interface OrderRepository extends JpaRepository<Orders, Long> {
        @Modifying
        @Transactional
        @Query("UPDATE Orders o SET o.status = :status WHERE o.id = :id")
        int updateDishById(@Param("id") Long id, @Param("status") Status status);

    }
