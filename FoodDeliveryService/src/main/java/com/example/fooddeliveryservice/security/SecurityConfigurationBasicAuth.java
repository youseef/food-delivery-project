package com.example.fooddeliveryservice.security;

import com.example.fooddeliveryservice.entity.enums.Roles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration

public class SecurityConfigurationBasicAuth {

    @Bean
    public PasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }
    @Bean
    public InMemoryUserDetailsManager userDetailsService(){
        UserDetails admin = User.
                builder().
                username("admin").
                password(encoder().encode("admin")).
                roles(Roles.ADMIN.name(), Roles.WAITER.name(), Roles.USER.name()).
                build();
        UserDetails waiter = User.builder()
                .username("waiter")
                .password(encoder().encode("waiter"))
                .roles(Roles.WAITER.name())
                .build();
        UserDetails user = User.builder()
                .username("user")
                .password(encoder().encode("user"))
                .roles(Roles.USER.name())
                .build();
        return new InMemoryUserDetailsManager(admin, waiter, user);
    }
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        http
                .csrf().disable().formLogin().disable()
                .cors().and()
                .authorizeHttpRequests((authz) -> authz.
                        requestMatchers("/api/").hasRole(Roles.ADMIN.name())
                        .requestMatchers("/dishes/**").hasRole(Roles.WAITER.name())
                        .requestMatchers("/orders/**").hasRole(Roles.USER.name())
                        .anyRequest().authenticated()
                )
                .httpBasic(Customizer.withDefaults());
        return http.build();
    }
}
