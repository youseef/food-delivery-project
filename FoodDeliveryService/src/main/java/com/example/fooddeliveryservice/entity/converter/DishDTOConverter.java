package com.example.fooddeliveryservice.entity.converter;

import com.example.fooddeliveryservice.entity.dtos.DishDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class DishDTOConverter implements AttributeConverter<DishDTO, String> {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(DishDTO dishDTO) {
        try {
            return objectMapper.writeValueAsString(dishDTO);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Error converting DishDTO to JSON string", e);
        }
    }

    @Override
    public DishDTO convertToEntityAttribute(String dishDTOJson) {
        try {
            return objectMapper.readValue(dishDTOJson, DishDTO.class);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Error converting JSON string to DishDTO", e);
        }
    }
}
