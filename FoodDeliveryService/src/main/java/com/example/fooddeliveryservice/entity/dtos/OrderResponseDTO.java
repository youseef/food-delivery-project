package com.example.fooddeliveryservice.entity.dtos;

import com.example.fooddeliveryservice.entity.Dish;
import com.example.fooddeliveryservice.entity.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderResponseDTO {
    private Long id;
    private List<DishDTO> dish;
    private Double totalCost;
    private Status status;
    private int distance;
    private int estimatedPreparationTime;
    private int estimatedDeliveryTime;
}
