package com.example.fooddeliveryservice.entity.enums;

public enum Status {
    PLACED,
    CANCELLED,
    DELIVERED

}
