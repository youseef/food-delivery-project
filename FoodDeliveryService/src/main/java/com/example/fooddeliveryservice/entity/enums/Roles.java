package com.example.fooddeliveryservice.entity.enums;

public enum Roles {
    ADMIN,
    WAITER,
    USER
}
