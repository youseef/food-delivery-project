package com.example.fooddeliveryservice.entity.dtos;

import com.example.fooddeliveryservice.entity.Dish;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DishDTO {
    private Long id;
    private String name;
    private Double price;
}
