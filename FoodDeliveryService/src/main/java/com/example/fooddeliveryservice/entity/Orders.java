package com.example.fooddeliveryservice.entity;

import com.example.fooddeliveryservice.entity.converter.DishDTOConverter;
import com.example.fooddeliveryservice.entity.dtos.DishDTO;
import com.example.fooddeliveryservice.entity.enums.Status;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long id;

    @Convert(converter = DishDTOConverter.class)
    private List<DishDTO> dish;

    private Double totalCost;
    private Status status;
    private int distance;
    private int estimatedPreparationTime;
    private int estimatedDeliveryTime;
}
