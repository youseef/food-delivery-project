package com.example.fooddeliveryservice.controller;

import com.example.fooddeliveryservice.entity.dtos.OrderResponseDTO;
import com.example.fooddeliveryservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.channels.ReadPendingException;
import java.util.List;

@RestController
@RequestMapping("/api/")
public class Controller {
    @Autowired
    private OrderService orderService;

    @GetMapping
    public ResponseEntity<List<OrderResponseDTO>> getAllOrders(){
        return ResponseEntity.ok(orderService.getAllOrders());
    }
}
