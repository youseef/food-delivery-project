package com.example.fooddeliveryservice.controller;

import com.example.fooddeliveryservice.entity.dtos.DishDTO;
import com.example.fooddeliveryservice.entity.dtos.OrderRequestDTO;
import com.example.fooddeliveryservice.entity.dtos.OrderResponseDTO;
import com.example.fooddeliveryservice.entity.enums.Status;
import com.example.fooddeliveryservice.service.DishService;
import com.example.fooddeliveryservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders/")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private DishService dishService;

    @PostMapping("place")
    public ResponseEntity<String> placeOrder(@RequestBody OrderRequestDTO order){
        orderService.placeOrder(order);
        return ResponseEntity.ok("Order has been placed");
    }

    @GetMapping
    public ResponseEntity<List<DishDTO>> getAllOrders(){
        return ResponseEntity.ok(dishService.getAllDishes());
    }
    @GetMapping("getOrderById/{id}")
    public ResponseEntity<OrderResponseDTO> getById(@PathVariable Long id){
        return ResponseEntity.ok(orderService.getOrderById(id));
    }
    @PutMapping("updateStatus/{id}")
    public ResponseEntity<String> updateOrderStatus(@PathVariable Long id, @RequestParam Status status){
        orderService.updateOrderStatus(id, status);
        return ResponseEntity.ok("the orders status has been updated");
    }


}
