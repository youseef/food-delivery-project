package com.example.fooddeliveryservice.controller;

import com.example.fooddeliveryservice.entity.dtos.DishDTO;
import com.example.fooddeliveryservice.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dishes/")
public class DishController {

    @Autowired
    private DishService dishService;

    @PostMapping("add")
    public ResponseEntity<String> addDish(@RequestBody DishDTO dishDTO) {
        dishService.addDish(dishDTO);
        return ResponseEntity.ok("Dish added successfully");
    }

    @PutMapping("update/{id}")
    public ResponseEntity<String> updateDish(@PathVariable Long id, @RequestBody DishDTO dishDTO) {
        dishService.updateDish(id, dishDTO);
        return ResponseEntity.ok("Dish updated successfully");
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<String> deleteDish(@PathVariable Long id) {
        dishService.deleteDish(id);
        return ResponseEntity.ok("Dish deleted successfully");
    }

    @GetMapping
    public ResponseEntity<List<DishDTO>> getAllDishes() {
        return ResponseEntity.ok(dishService.getAllDishes());
    }

    @GetMapping("getOneDish/{id}")
    public ResponseEntity<DishDTO> getDishById(@PathVariable Long id) {
        return ResponseEntity.ok(dishService.getDishById(id));
    }
}
